# Project 1 Testing script instructions

`Test.sh` needs to be run with the following command line pattern: `Test.sh {name of your CPP file}`.

`Test.sh` will compile your program with g++ and then run through the test cases and copy the output of each into output.txt.

**NOTE due to encoding issues with redirecting console output to file, output.txt might appear as corrupt to some text edditors please reference terminal output instead.**