# CS 101 Test Scripts

## Instructions for running Bash scrips
for all options, your project file needs to be in the same folder as the script.

### On Windows
1. Using Cygwin:
   1. In Cygwin, navigate to the folder with the script
   2. enter `./{name of the script}.sh` followed by any command line args required.
2. Using WSL:
   1. See the Linux instructions.

### On Mac:
1. Navigate to the folder with the script
2. In the terminal run `bash {name of the script}.sh` followed by any command line args required

### On Linux
1. Using the terminal, navigate to the folder with the script
2. In the terminal run `chmod +x {name of the script}.sh` to make the file executable
3. run `./{name of the script}.sh` followed by any command line args required